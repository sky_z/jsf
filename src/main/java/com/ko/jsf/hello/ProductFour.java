package com.ko.jsf.hello;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class ProductFour {

	// fields
	private String name;
	private String shippingCountry;

	// no-arg constructor
	public ProductFour() {

		// pre-populate the bean
		name = "Tea";
		shippingCountry = "Austria";

	}

	// getters and setters
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShippingCountry() {
		return shippingCountry;
	}

	public void setShippingCountry(String shippingCountry) {
		this.shippingCountry = shippingCountry;
	}

}
