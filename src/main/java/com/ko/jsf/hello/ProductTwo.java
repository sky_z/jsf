package com.ko.jsf.hello;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class ProductTwo {

	// fields
	private String name;
	private String category;
	private String shippingCountry;

	// list of categories for the drop-down list
	List<String> categoryOptions;

	// no-arg constructor
	public ProductTwo() {
		// fill the list of categories
		categoryOptions = new ArrayList<>();

		categoryOptions.add("Food");
		categoryOptions.add("Clothes");
		categoryOptions.add("Electronic");
		categoryOptions.add("Games");
	}

	// getters and setters

	// getter method for category options

	public List<String> getCategoryOptions() {
		return categoryOptions;
	}

	public String getShippingCountry() {
		return shippingCountry;
	}

	public void setShippingCountry(String shippingCountry) {
		this.shippingCountry = shippingCountry;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

}
